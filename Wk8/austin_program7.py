"""
Program name:   austin_program7.py
Program Description: Prints a chart of windchill temperatures using a nested while loop
Author: Mark Austin
Date created: March, 20 2021

Notes: Uses the python time library

"""
import time

# take in temperature and wind velocity and calculates wind chill
def windChill(t, v):
    if v > 3:
        chill = 35.74 + 0.6215 * t - 35.75 * (v ** .16) + .4275 * t * (v ** .16)
    else:
        chill = t
    return chill


def main():
    """
    Begin Code for assignment

    """
    velocity = 0
    # start of the table
    print("Wind Chill Table\n\n")
    print("                         Temperature")
    print("MPH|   -20   -10     0    10    20    30    40    50    60")
    print("---+------------------------------------------------------")
    # loop through the wind speeds
    while velocity <= 50:
        print("{0:>3}|".format(velocity), end="")
        temp = -20
        # fill out the row with the temps for that wind speed
        while temp <= 60:
            print("{0:>6}".format(round(windChill(temp, velocity))), end="")
            temp += 10
        #end the line and incrament the temp for the main loop
        print()
        velocity += 5
    print("\n\n")
    """
    End Code for assignment
    """
    """
    Begin code for template
    """
    # Prints Author's name, class, and date
    print("Mark Austin")
    print("CIS 110")
    # this function will print the current date and time using asctime()
    print(time.asctime(time.localtime(time.time())))
    """
    End Code for template
    """


if __name__ == "__main__":
    main()
