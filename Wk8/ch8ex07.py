# ch8ex07.py
# find the 2 primes that add up to the given even number

from ch8ex05 import isPrime

def goldbach(x):
    candidate = 3
    while candidate < x/2:
        other = x - candidate
        if isPrime(candidate) and isPrime(other):
            return candidate, other
        candidate = candidate + 2


def main():
    print("Goldbach checker\n")

    n = int(input("Enter an even natural number: "))
    if n % 2 != 0:
        print(n, "is not even!")
    else:
        prime1, prime2 = goldbach(n)
        print("{0} + {1} = {2}".format(prime1, prime2, n))

if __name__ == '__main__':
    main()