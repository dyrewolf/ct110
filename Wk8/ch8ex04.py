# ch8ex04.py
# calculate the Syracuse sequence

def main():
    print("Calculate the Syracuse sequence\n")
    n = int(input("Enter the initial value (an int >= 1): "))
    while n!= 1:
        print(n, end=' ')
        if n % 2 == 0:
            n = n /2
        else:
            n = 3* n + 1
    print(1)

if __name__ == '__main__':
    main()