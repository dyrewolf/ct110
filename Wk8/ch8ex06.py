# ch8ex06.py

from ch8ex05 import isPrime

def main():
    print("This program finds all prime numbers up to N\n")
    n = int(input("Enter the upper limit, n: "))
    print("primes: ", end=' ')
    for i in range(1,n+1):
        if isPrime(i):
            print(i, end=' ')
    print("Done")

if __name__ == '__main__':
    main()