"""
Program name:   CIS110 Program Template + Hello World
Program Description: Prints Hello World and uses the template for CIS assignments
Author: Mark Austin
Date created: January 30, 2021

Notes: Uses the python time library

"""
import time

def main():
    """
    Begin Code for assignment

    """
    #Prints Hello World for the assignment
    print("Hello World")
    """
    End Code for assignment
    """
    """
    Begin code for template
    """
    #Prints Autor's name, class, and date
    print("Mark Austin")
    print("CIS 110")
    #this function will print the current date and time using asctime()
    print(time.asctime( time.localtime(time.time())))
    """
    End Code for template
    """

main()