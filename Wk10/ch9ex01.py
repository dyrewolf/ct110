# ch9ex01.py
# grnerate random numbers
from random import random

def main():
    printIntro()
    n = getInputs()
    printNumbers(n)


def printIntro():
    print("This will print n random numbers")


def getInputs():
    return eval(input("How many numbers should I print? "))

def printNumbers(n):
    for x in range(n):
        print("{0:0.5f}".format(random()))

if __name__ == "__main__":
    main()

