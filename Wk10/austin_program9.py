"""
Program name:   CIS110 Program Template + Hello World
Program Description: Prints Hello World and uses the template for CIS assignments
Author: Mark Austin
Date created: January 30, 2021

Notes: Uses the python time library

"""
import time
from random import *

def main():
    """
    Begin Code for assignment
    """
    seed(123456)
    printIntro()
    n = getInput()
    playGames(n)

    """
    End Code for assignment
    """
    """
    Begin code for template
    """
    # Prints Author's name, class, and date
    print("Mark Austin")
    print("CIS 110")
    # this function will print the current date and time using asctime()
    print(time.asctime(time.localtime(time.time())))
    """
    End Code for template
    """

def printIntro():
    print("This program simulates N Paper, Rock, Scissors matches using 3 strategies")

def getInput():
    return int(input("enter the number of games to simulate: "))

def playGames(n):
    # choose at random
    winsRand = playRandom(n)
    # only play Rock
    winsRock = playRock(n)
    # Pick what the opponent played last
    # first game is random
    winsLast = playLast(n)
    printResults(winsRand, winsRock, winsLast, n)

def printResults(winsRand, winsRock, winsLast, n):
    print("Playing Randomly, Vinnie wins {0:0.2%} of his matches".format(winsRand / n))
    print("Playing only Rock, Vinnie wins {0:0.2%} of his matches".format(winsRock / n))
    print("Playing Previous Move, Vinnie wins {0:0.2%} of his matches".format(winsLast / n))

def playRandom(n):
    # Rock=1, Paper=2, Scissors=3
    wins = 0
    for i in range(n):
        player = 0
        opponent = 0
        while player == opponent:
            player = randrange(1, 4)
            opponent = randrange(1, 4)
        if playerWins(player, opponent):
            wins = wins + 1
    return wins

def playRock(n):
    # Rock=1, Paper=2, Scissors=3
    wins = 0
    for i in range(n):
        player = 1
        opponent = 1
        while opponent == 1:
            opponent = randrange(1, 4)
        if playerWins(player, opponent):
            wins = wins + 1
    return wins

def playLast(n):
    # Rock=1, Paper=2, Scissors=3
    wins = 0
    last = 0
    for i in range(n):
        player = 0
        opponent = 0
        while player == opponent:
            if n == 0:
                player = randrange(1, 4)
            else:
                player = last
            opponent = randrange(1, 4)
        last = opponent
        if playerWins(player, opponent):
            wins = wins +1
    return wins

def playerWins(player, opponent):
    # Rock=1, Paper=2, Scissors=3
    if (player == 1 and opponent == 3) or (player == 2 and opponent == 1) or (player == 3 and opponent == 2):
        return True
    else:
        return False


if __name__ == "__main__":
    main()
