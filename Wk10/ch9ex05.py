#ch9ex05.py
# powerball lottery

from random import *

def main():
    printIntro()
    n = getInputs()
    simNDraws(n)

def printIntro():
    print("This program will simulate the powerball drawing")

def getInputs():
    return int(input("How many draws to simulate? "))

def simNDraws(n):
    for i in range(n):
        simDraw()

def simDraw():
    fivenumbers = drawFiveNumbers()
    powerball = drawPowerball()
    printDraw(fivenumbers, powerball)

def drawFiveNumbers():
    fivenumbers = []
    while len(fivenumbers) < 5:
        ball = drawBall(1, 69)
        if ball in fivenumbers:
            continue
        fivenumbers = fivenumbers + [ball]
    return fivenumbers

def drawPowerball():
    return drawBall(1, 26)

def drawBall(lo, hi):
    return randrange(lo, hi+1)

def printDraw(fivenumbers, powerball):
    print(fivenumbers, powerball)

if __name__ == '__main__':
    main()