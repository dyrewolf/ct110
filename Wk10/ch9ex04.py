#ch9ex04.py
# Craps game

from random import randrange, seed

def main():
    printIntro()
    n = getInputs()
    wins = playGames(n)
    printResults(wins, n)

def printIntro():
    print("this program estimates the probability of winning a craps game")

def getInputs():
    return int(input("How many games should I simulate? "))

def playGames(n):
    wins = 0
    for i in range(n):
        if winCraps():
            wins = wins + 1
    return wins

def winCraps():
    roll = rollDice()
    if roll == 7 or roll == 11:
        return True
    elif roll == 2 or roll == 3 or roll == 12:
        return False
    else:
        return rollForPoint(roll)

def rollForPoint(point):
    roll = rollDice()
    while roll != 7 and roll != point:
        roll = rollDice()
    return roll == point

def rollDice():
    return randrange(1, 7) + randrange(1, 7)

def printResults(wins, n):
    print("\nThe player wins,", wins, "games.")
    print("The estimated probability of a win in {0:0.2%}".format(wins/n))

if __name__ == '__main__':
    main()
