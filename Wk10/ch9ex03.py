# ch9ex03.py
# testing uniform distribution

from random import randrange, seed

def main():
    seed(123456)
    printIntro()
    n = getInputs()
    results = generateSamples(n)
    printSummary(results)

def printIntro():
    print("This program tests if random is uniformly distributed")

def getInputs():
    return eval(input("How many samples should I take? "))

def generateSamples(n):
    results = [0,0,0,0,0,0]
    for x in range(n):
        sample = randrange(0,6)
        results[sample] = results[sample] +1
    return results

def printSummary(results):
    totalsamples = sum(results)
    for i in range(0,6):
        print("{}: {:6.2%}".format(i, results[i] / totalsamples))

if __name__ == "__main__":
    main()

