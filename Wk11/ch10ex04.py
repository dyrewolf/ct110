#ch10ex04.py
# 3 Button monte

from random import randrange
from graphics import *
from button import Button


def main():

    win = GraphWin("Three Button Monte", 350, 200)
    win.setCoords(.5, 0, 3.5, 6)
    b1 = Button(win, Point(1, 5), .75, 1, "Door 1")
    b1.activate()
    b2 = Button(win, Point(2, 5), .75, 1, "Door 2")
    b2.activate()
    b3 = Button(win, Point(3, 5), .75, 1, "Door 3")
    b3.activate()
    mess = Text(Point(2, 3.75), "Guess a door.")
    mess.setStyle("bold")
    mess.draw(win)

    scoreboard = Text(Point(2, .5), "")
    scoreboard.draw(win)

    quitBtn = Button(win, Point(1.5, 1.5), .75, 1, "Quit")
    playBtn = Button(win, Point(2.5, 1.5), .75, 1, "Play Again")

    losses = 0
    wins = 0
    while True:
        quitBtn.deactivate()
        playBtn.deactivate()
        secret = randrange(1, 4)
        mess.setText("Guess a door.")

        choice = None
        while choice == None:
            pt = win.getMouse()
            for button in [b1, b2, b3]:
                if button.clicked(pt):
                    choice = button

        choiceNum = int(choice.getLabel() [-1])
        if choiceNum == secret:
            mess.setText("You win!")
            wins += 1
        else:
            mess.setText("You loose. The answer was door {0}.".format(secret))
            losses += 1
        scoreboard.setText("Wins: {} Losses: {}".format(wins, losses))
        quitBtn.activate()
        playBtn.activate()
        while True:
            pt = win.getMouse()
            if quitBtn.clicked(pt) or playBtn.clicked(pt): break
        if quitBtn.clicked(pt): break

    win.close()


if __name__ == '__main__': main()