"""
Program name:   CIS110 Program Template + Hello World
Program Description: Prints Hello World and uses the template for CIS assignments
Author: Mark Austin
Date created: January 30, 2021

Notes: Uses the python time library

"""
import time

class Account:
    def __init__(self, label):
        self.name = label
        self.total = 0

    def deposit(self, amount):
        if amount < 0:
            print("Error: cannot deposit negative amounts")
        else:
            print("depositing ${0} Into {1}".format(amount, self))
            self.total = self.total + amount
        return

    def withdraw(self, amount):
        if self.total < amount:
            print("Error: ${0} exceeds the current balance".format(amount))
        else:
            print("withdrawing ${0} from {1}".format(amount, self))
            self.total = self.total - amount
        return

    def balance(self):
        return self.total

    def transfer(self, amount, ID):
        if self.total < amount:
            print("Error: ${0} exceeds the current balance".format(amount))
        elif amount < 0:
            print("Error: cannot transfer negative amounts")
        else:
            if ID == "cash":
                print("Withdrawing ${0} from {1} to Cash".format(amount, self))
                self.withdraw(amount)
                return amount
            else:
                print("Transfering ${0} from {1} to {2}".format(amount, self, ID))
                ID.deposit(amount)
            self.withdraw(amount)

    def __str__(self):
        if self.name == "check":
            nameStr = "Checking"
        elif self.name == "save":
            nameStr = "Savings"
        return "{}".format(nameStr)

def printBalances(checking, savings, cash):
    print("Checking: ${0}  Savings: ${1}  Cash: ${2}\n".format(checking.balance(), savings.balance(), cash))


def main():
    """
    Begin Code for assignment

    """
    cash = 0
    savings = Account("save")
    checking = Account("check")


    print("Starting Balances:\n")
    printBalances(checking, savings, cash)
    checking.deposit(500)
    printBalances(checking, savings, cash)
    savings.deposit(100)
    printBalances(checking, savings, cash)
    checking.transfer(150, savings)
    printBalances(checking, savings, cash)
    checking.deposit(-50)
    printBalances(checking, savings, cash)
    checking.withdraw(600)
    printBalances(checking, savings, cash)
    cash = cash + checking.transfer(250, "cash")
    printBalances(checking, savings, cash)
    cash = cash + savings.transfer(100, "cash")
    printBalances(checking, savings, cash)
    savings.transfer(75, checking)
    printBalances(checking, savings, cash)



    """
    End Code for assignment
    """
    """
    Begin code for template
    """
    # Prints Author's name, class, and date
    print("Mark Austin")
    print("CIS 110")
    # this function will print the current date and time using asctime()
    print(time.asctime(time.localtime(time.time())))
    """
    End Code for template
    """


if __name__ == "__main__":
    main()
