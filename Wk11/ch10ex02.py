#ch10ex02.py
# Cats

from graphics import *
from button import *

class Cat:
    def __init__(self, win, center):
        self.x = x = center.getX()
        self.y = y = center.getY()
        self.nose = Circle(Point(x, y), 0.25)
        self.leye = Circle(Point(x-1, y-1), .5)
        self.reye = Circle(Point(x+1, y-1), .5)
        self.mouth = Line(Point(x-1, y+1), Point(x+1, y+1))
        self.lear = Polygon(Point(x-1.75, y-1), Point(x-2.25, y-4), Point(x-1.25, y-2))
        self.rear = Polygon(Point(x+1.75, y-1), Point(x+2.25, y-4), Point(x+1.25, y-2))
        self.head = Oval(Point(x-2, y-2.5), Point(x+2, y+2.25))

        self.lear.draw(win)
        self.rear.draw(win)

        self.head.draw(win)
        self.head.setFill("white")
        self.nose.draw(win)
        self.nose.setFill("pink")
        self.leye.draw(win)
        self.reye.draw(win)
        self.mouth.draw(win)

    def move(self, newCenter):
        newX, newY = newCenter.getX(), newCenter.getY()
        self.nose.move(newX - self.x, newY - self.y)
        self.leye.move(newX - self.x, newY - self.y)
        self.reye.move(newX - self.x, newY - self.y)
        self.mouth.move(newX - self.x, newY - self.y)
        self.head.move(newX - self.x, newY - self.y)
        self.lear.move(newX - self.x, newY - self.y)
        self.rear.move(newX - self.x, newY - self.y)
        self.x = newX
        self.y = newY

def main():
    win = GraphWin("Cats", 640, 480)
    win.setCoords(0, 24, 32, 0)

    myCat = Cat(win, Point(16, 12))

    myBtn = Button(win, Point(16, 22), 5, 2, "Quit")
    myBtn.activate()

    while True:
        where = win.getMouse()
        if myBtn.clicked(where): break
        myCat.move(where)

    win.close()

if __name__ == "__main__": main()