#ch10ex01.py
# Bozo class from discussion 3

class Bozo:
    def __init__(self, value):
        print("Creating a Bozo from: ", value)
        self.value = 2 * value

    def clown(self, x):
        print("Clowning: ", x)
        print("  ->", x * self.value)
        return x + self.value

def main():
    print ("Clowning Around")
    c1 = Bozo(3)
    c2 = Bozo(4)
    print(c1.clown(3))
    print(c2.clown(c1.clown(2)))

main()