# ch7ex01.py
# calculate wage based on number of hours worked

def main():
    print("Weekly pay calculator\n")
    hours = float(input("Enter hours worked: "))
    wage = float(input("Enter hourly wage: "))
    if hours <= 40:
        pay = hours * wage
    else:
        pay = 40 * wage + (hours-40) * 1.5 * wage

    print("Your week's pay is ${0:0.2f}".format(pay))

if __name__ == '__main__':
    main()

