# ch5ex03.py
# convert number grade to letter grade for quiz scorse

def main():
    print("Exam grader\n")
    score = int(input("Enter the score (out of 100): "))
    if score >= 90:
        grade = "A"
    elif score >= 80:
        grade = "B"
    elif score >= 70:
        grade = "C"
    elif score >= 60:
        grade = "D"
    else:
        grade = "F"
    print("The grade is", grade)


if __name__ == "__main__":
    main()
