# ch5ex02.py
# convert number grade to letter grade for quiz scorse

def main():
    print("Five point quiz grader")
    score = int(input("Enter the score: "))
    if score <= 1:
        grade = "F"
    elif score == 2:
        grade = "D"
    elif score == 3:
        grade = "C"
    elif score == 4:
        grade = "B"
    else:
        grade = "A"
    print("The grade is", grade)


if __name__ == "__main__":
    main()
