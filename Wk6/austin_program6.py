"""
Program name:   austin_program6
Program Description: validates password based on requirements then encrypts
Author: Mark Austin
Date created: March 7, 2021

Notes: Uses the python time library

"""
import time


def passwordCheck(password):
    invalid = False
    if not lengthTest(password):
        invalid = True
    if not upperCaseTest(password):
        invalid = True
    if not lowerCaseTest(password):
        invalid = True
    if not digitTest(password):
        invalid = True
    if not specialCharacterTest(password):
        invalid = True
    if invalid:
        # if any of the checks failed the overall check fails
        return
    else:
        return True


def lengthTest(password):
    # Verifies the password length is at least 8 characters
    if len(password) >= 8:
        return True
    else:
        print("Password is only " + str(len(password)) + "characters long")
        print("Password must be at least 8 characters long")
        return


def upperCaseTest(password):
    # check if the string password contains an upper case letter
    for char in password:
        if char.isupper():
            return True
    print("Password must contain at least 1 upper case letter")
    return


def lowerCaseTest(password):
    # check if the string password contain a lowercase letter
    for char in password:
        if char.islower():
            return True
    print("Password must contain at least 1 lower case letter")
    return


def digitTest(password):
    # check if there is a digit in the string password
    for char in password:
        if char.isnumeric():
            return True
    print("Password must contain at least 1 digit")
    return


def specialCharacterTest(password):
    matches = ["!", "@", "#", "$"]
    # Check if any for the characters in matches are in the string password
    if any(x in password for x in matches):
        return True
    print("Password must contain at least 1 of these four special characters: ! @ # $")
    return


def encrypt(password):
    cipher = ""
    for letter in password:
        # add +1 if it is alphanumeric. For example, if the character is a uppercase letter,
        # lower case letter, or a digit: encryptedCharacter = character + 1
        if letter.isupper():
            cipher = cipher + chr((ord(letter) + 1 - ord("A")) % 26 + ord("A"))
        elif letter.islower():
            cipher = cipher + chr((ord(letter) + 1 - ord("a")) % 26 + ord("a"))
        elif letter.isdigit():
            cipher = cipher + chr(ord(letter) + 1)
        # add -1 if it is one of the four special characters.
        # For example, if the character is one of these four characters ! @ # $:
        # encryptedCharacter = character - 1
        else:
            cipher = cipher + chr(ord(letter) - 1)

    print("Your selected password is valid and encrypted it is: ", cipher)
    return


def main():
    """
    Begin Code for assignment

    """
    print("this program will validate the entered password/n")
    print("and return the encrypted value if accepted/n/n")
    print("Passwords require: \n\
    at least 8 characters long\n\
    at least 1 upper case letter\n\
    at least 1 lower case letter\n\
    at least 1 digit\n\
    at least 1 of these four special characters: ! @ # $\n")
    password = input("Please enter your desired password:")
    if passwordCheck(password):
        encrypt(password)
    else:
        print("Your selected password is invalid. Please resubmit")

    """
    End Code for assignment
    """
    """
    Begin code for template
    """
    # Prints Author's name, class, and date
    print("Mark Austin")
    print("CIS 110")
    # this function will print the current date and time using asctime()
    print(time.asctime(time.localtime(time.time())))
    """
    End Code for template
    """


if __name__ == "__main__":
    main()
