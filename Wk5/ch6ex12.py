# ch6ex12.py
# sums the numbers in a list

def sumList(nums):
    total = 0
    for n in nums:
        total = total + n
    return total

def test():
    n = int(input("How many numbers should I sum? "))
    nums = list(range(1, n+1))
    print("the sum of the first ", n, "numbers is", sumList(nums))

test()
