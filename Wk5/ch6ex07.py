# ch6ex07.py
#   compute the nth Fibonacci number

def fibo(n):
    curr, prev = 1, 1
    for i in range(n - 2):
        curr, prev = curr + prev, curr
    return curr

def main():
    print("This program calculates the nth Fibonacci value.")
    n = int(input("Enter the value of n: "))
    print("The nth Fibonacci number is", fibo(n))


main()
