# ch6ex06.py
# calculate the area of a triangle

import math

def triArea(a, b,c,):
    s = (a+b+c)/2.0
    return math.sqrt(s*(s-a)*(s-b)*(s-c))

def main():
    print("This program calculates the area of a triangle.")
    print()

    a,b,c=eval(input("Enter the length of each side (a,b,c): "))

    print()
    print("The area is", triArea(a,b,c), "square units.")

main()
