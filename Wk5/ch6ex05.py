#ch3ex02.py
#   calculate the cost of a pizza in cents / sq inch

import math
def costPer(d, price):
    return float(price) / area(d)

def area(d):
    return  math.pi * (0.5*d)**2
def main():
    print("Program to calculater the value of a pizza\n")
    print()

    diam = float(input("Enter the diameter of the pizza: "))
    cost = float(input("Enter the price of the pizza: "))

    print("\nThe pizza costs %0.4f per square unit." % (costPer(diam, cost)))


main()
