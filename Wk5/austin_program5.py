"""
Program name:   austin_program5.py
Program Description: sums a list of numbers and the sum of the squares
                     of those numbers
Author: Mark Austin
Date created: Feb 28, 2021

Notes: Uses the python time library

"""
import time


# function adds the values contained in the list passed to it
def sumList(nums):
    total = 0
    for num in nums:
        total = total + num
    return total


# function replaces the values in a list with the squares of those values
def squareEach(nums):
    # using an accumulator for a counter
    # as I don't think we have covered the enumerate() function
    i = 0
    for num in nums:
        # updating the nums list to contain the new values for the squares
        nums[i] = num**2
        i += 1


def main():
    """
    Begin Code for assignment

    """
    print("This program will sum all numbers to N")
    print("and the squares of those numbers")
    print()
    # enter user input into a list
    n = int(input("Enter a value for N:"))
    nums = list(range(1, n+1))
    print()
    print("your list of number is:", nums)
    print("the sum is:", sumList(nums))
    print()
    squareEach(nums)
    # the list of nums has been modified by the squareEach() function
    print("your list of squares is:", nums)
    print("the sum of the squares is:", sumList(nums))
    print()

    """
    End Code for assignment
    """
    """
    Begin code for template
    """
    # Prints Author's name, class, and date
    print("Mark Austin")
    print("CIS 110")
    # this function will print the current date and time using asctime()
    print(time.asctime(time.localtime(time.time())))
    """
    End Code for template
    """


main()
