def main():
    print("This program calculates the future value of a yearly investment")
    print()

    payment = eval(input("Enter the ammount to invest each year:"))
    apr = eval(input("Enter the annual interest rate: "))
    years = eval(input("Enter the number of years: "))

    principal = 0.0
    for i in range(years):
        principal = (principal + payment) * (1 + apr)

    print("The value in", years, "years is:", principal)

main()