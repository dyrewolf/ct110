"""
Program name:   CIS110 Program Mars rover transmission time calculator
Program Description: Calculates and prints the time to transmit from the Mars rover to earth
    From Furthest, Closest, and the Average distance
Author: Mark Austin
Date created: Feb 7, 2021

Notes: Uses the python time library

"""
import time

def main():
    """
    Begin Code for assignment
    """
    lightSpeed = 186000 #light speed in miles per second
    maxDistance = 249000000 #max distance in miles
    minDistance = 34000000 #min distance in miles
    avgDistance = 139000000 #average distance in miles

    #time = distance/speed
    maxTransmitTime = maxDistance / lightSpeed
    minTransmitTime = minDistance / lightSpeed
    avgTransmitTime = avgDistance / lightSpeed

    print("The Mars rover transmits data at light speed")
    print()
    print("The amount of time for data to reach earth from,")
    #added \t to clean up the output
    print("The furthest point is : \t", maxTransmitTime, "Seconds")
    print("The closest point is : \t\t", minTransmitTime, "Seconds")
    print("The average distance is : \t", avgTransmitTime, "Seconds")
    print()
    """
    End Code for assignment
    """
    """
    Begin code for template
    """
    #Prints Autor's name, class, and date
    print("Mark Austin")
    print("CIS 110")
    #this function will print the current date and time using asctime()
    print(time.asctime( time.localtime(time.time())))
    """
    End Code for template
    """

main()