def main():
    print("This program computes the average of three exam scores.")

    s1, s2, s3 = eval(input("Enter three scores separated by a comma: "))
    average = (s1 + s2 + s3) / 3

    print("The average of the scores is:", average)
    input("Press the <Enter> key to quit.")

main()