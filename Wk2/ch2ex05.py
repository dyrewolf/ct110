def main():
    print("Celsius  Fahrenheit")
    print("-------------------")
    for celsius in [0,10,20,30,40,50,60,70,80,90,100]:
        fahrenheit = 9.0 / 5.0 * celsius + 32
        print(celsius, "        ", fahrenheit)

    input("Press the <Enter> key to quit.")

main()