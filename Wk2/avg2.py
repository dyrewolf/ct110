def main():
    print("This program computes the average of two exam scores.")

    s1, s2 = eval(input("Enter two scores separated by a comma: "))
    average = (s1 + s2) / 2

    print("The average of the scores is:", average)

main()