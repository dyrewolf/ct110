# ch5ex07.py
# encrypt and decrypt using a Caesar cipher

def main():
    print("Caesar cipher")
    print()

    key = int(input("Enter the key value: "))
    plain = input("Enter the phrase to encode: ")
    cipher = ""
    decrypt = ""
    for letter in plain:
        cipher = cipher + chr(ord(letter) + key)

    for letter in cipher:
        decrypt = decrypt + chr(ord(letter) - key)

    print("Original message was: ", plain)
    print("Encoded message follows: ", cipher)
    print("Decrypted message follows: ", decrypt)


main()
