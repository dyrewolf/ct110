"""
Program name:   Caesar Cipher
Program Description: encrypts and decrypts a caesar cipher
Author: Mark Austin
Date created: Feb 21, 2021

Notes: Uses the python time library

"""
import time

def main():
    """
    Begin Code for assignment

    """
    print("Caesar cipher")
    print()

    key = int(input("Enter the key value: "))
    plain = (input("Enter the phrase to encode: "))
    # removes the spaces from the original message
    toEncrypt = "".join(plain.split())
    cipher = ""
    decrypt = ""
    print(plain)
    # encrypt message
    for letter in toEncrypt:
        # test for uppercase letters
        if letter.isupper():
            cipher = cipher + chr((ord(letter) + key - ord("A")) % 26 + ord("A"))
        else:
            cipher = cipher + chr((ord(letter) + key - ord("a")) % 26 + ord("a"))

    # decrypt cipher
    for letter in cipher:
        # test for uppercase letters
        if letter.isupper():
            decrypt = decrypt + chr((ord(letter) - key - ord("A")) % 26 + ord("A"))
        else:
            decrypt = decrypt + chr((ord(letter) - key - ord("a")) % 26 + ord("a"))

    print("Original message was: ", plain)
    print("Encoded message follows: ", cipher)
    print("Decrypted message follows: ", decrypt)


    """
    End Code for assignment
    """
    """
    Begin code for template
    """
    #Prints Autor's name, class, and date
    print("Mark Austin")
    print("CIS 110")
    #this function will print the current date and time using asctime()
    print(time.asctime( time.localtime(time.time())))
    """
    End Code for template
    """

main()