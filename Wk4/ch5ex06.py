# ch5ex06.py
# calculates the number value of a name

def main():
    print("This program computes the 'number value' of a name")
    print()

    name = input("Enter a single name: ")
    letters = "".join(name.split())
    total = 0
    for letter in letters:
        total = total + ord(letter.lower()) - ord('a') +1

    print("The value is:", total)


main()
