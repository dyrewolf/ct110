# ch3ex06.py
#   Calculate the distance between 2 points
import math

def main():
    print("This program calculates the slope of a line.")
    print()

    x1 = float(input("Enter the x for the first point: "))
    y1 = float(input("Enter the y for the first point: "))
    print()
    x2 = float(input("Enter the x for the second point: "))
    y2 = float(input("Enter the y for the second point: "))

    distance = math.sqrt((x2-x1)**2 + (y2-y1)**2)

    print()
    print("The distance between the points is", distance)


main()
