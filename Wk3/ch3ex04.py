# ch3ex04.py
#   calculate the distance of a lightning strike
#   based on difference in time from flash to thunder

def main():
    print("This program calculates the distance from a lightning strike.")
    print()

    seconds = int(input("Enter the number of seconds between flash and crash: "))
    feet = 1100 * seconds
    miles = feet / 5280.0

    print()
    print("The lightning is approximately", round(miles, 1), "miles away.")


main()
