# ch3ex11.py
#   find the average of the numbers entered

def main():
    print("Program to calculate the average")
    print()

    n = int(input("How many numbers do you have?  "))
    total = 0
    for i in range(n):
        num = float(input("Enter a number: "))
        total = total + num

    print()
    print("The average of the numbers is", total/n)


main()
