"""
Program name:   Program 3
Program Description: calculates a number raised to a power
Author: Mark Austin
Date created: Feb 14, 2021

Notes: Uses the python time library

"""
import time
import math

def main():
    """
    Begin Code for assignment

    """
    print("This program will calculate a number raised to a power")
    print()
    x = int(input("Enter number to be raised: "))
    y = int(input("Enter the value for the exponent: "))

    answer = int(math.pow(x,y))
    print()
    print("The answer of", x, "to the power of", y, "is: ", answer)

    """
    End Code for assignment
    """
    """
    Begin code for template
    """
    #Prints Autor's name, class, and date
    print("Mark Austin")
    print("CIS 110")
    #this function will print the current date and time using asctime()
    print(time.asctime( time.localtime(time.time())))
    """
    End Code for template
    """

main()