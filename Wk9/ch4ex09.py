#ch4ex09.py
#create rectangle from mouse clicks

from graphics import *
import math

def main():

    win = GraphWin("Rectangle", 500, 500)
    msg = Text(Point(250, 450), "Click opposite corners of a rectangle").draw(win)

    p1 = win.getMouse()
    p1.draw(win)
    p2 = win.getMouse()

    rect = Rectangle(p1, p2)
    rect.setWidth(2)
    rect.draw(win)

    length = abs(p2.getX()-p1.getX())
    height = abs(p2.getY()-p1.getY())
    area = length * height
    perimeter = 2 * (length + height)

    msg.setText("The perimeter is {:.0f} pixels".format(perimeter))
    Text(Point(250,475), "The area is {:.0f} pixels".format(area)).draw(win)

    win.getMouse()
    win.close()

main()