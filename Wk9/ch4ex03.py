# ch4ex03.py
# draw a face

from graphics import *

def main():
    #create window
    win = GraphWin("face", 500, 500)
    win.setCoords(-10, -10, 10, 10)

    # create face
    oval = Oval(Point(-4, -4), Point(4, 8))
    oval.setFill("grey")
    e1 = Circle(Point(-2, 4), 2)
    p1 = Circle(Point(-3, 3), 1)
    p1.setFill("Black")
    e1.setFill("white")
    e2 = Circle(Point(2, 4), 2)
    p2 = Circle(Point(1, 3), 1)
    e2.setFill("white")
    p2.setFill("black")
    nose = Polygon(Point(-1, 1), Point(0, 0), Point(1, 1))
    nose.setFill("brown")
    mouth = Line(Point(-1, -2), Point(1, -2))
    mouth.setOutline("red")

    #draw face
    oval.draw(win)
    e1.draw(win)
    p1.draw(win)
    e2.draw(win)
    p2.draw(win)
    nose.draw(win)
    mouth.draw(win)
    win.getMouse()
    win.close()


main()
