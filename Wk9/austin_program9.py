"""
Program name:   austin_program9py
Program Description: Prints Hello World and uses the template for CIS assignments
Author: Mark Austin
Date created: March 28, 2021

Notes: Uses the python time library

"""
from graphics import *
import math
import time


def drawX(selection, win):
    line1 = Line(Point(math.ceil(selection.getX())-.75, math.ceil(selection.getY())-.25), Point(math.ceil(selection.getX())-.25, math.ceil(selection.getY())-.75))
    line2 = Line(Point(math.ceil(selection.getX()) - .75, math.ceil(selection.getY()) - .75),
                 Point(math.ceil(selection.getX()) - .25, math.ceil(selection.getY()) - .25))
    line1.setWidth(3)
    line2.setWidth(3)
    line1.draw(win)
    line2.draw(win)
    return

def drawY(selection, win):
    circ = Circle(Point(math.ceil(selection.getX())-.5,math.ceil(selection.getY())-.5), .25)
    circ.setWidth(3)
    circ.draw(win)
    return


def gameTurn(win, turn):
    selection = win.getMouse()
    print(selection)
    errorMsg = Text(Point(2.5, .75), "")
    errorMsg.draw(win)
    while (selection.getX() <= 1 or selection.getX() >= 4) or (selection.getY() <= 1 or selection.getY() >= 4):
        errorMsg.setText("Error: selection outside of game board. Try again")
        selection = win.getMouse()
        print(selection)
    errorMsg.setText("")
    if turn == "X":
        drawX(selection, win)
    else:
        drawY(selection, win)
    return


def main():
    """
    Begin Code for assignment

    """
    win = GraphWin("Tic-Tac-Toe", 500, 500)
    win.setCoords(0, 0, 5, 5)
    turn = "X"
    turnLabel = Text(Point(2.5, .5), "Click Square for {}".format(turn))
    turnLabel.draw(win)
    gameLabel = Text(Point(2.5, 4.5), "Let's Play Tic-Tac-Toe")
    gameLabel.draw(win)
    for i in range(1, 4):
        for j in range(1, 4):
            rect = Rectangle(Point(i, j), Point(i+1, j+1))
            rect.setFill('yellow')
            rect.draw(win)
    movesLeft = 9
    while (movesLeft > 0):
        gameTurn(win,turn)
        if turn == "X":
            turn = "O"
        else:
            turn = "X"
        turnLabel.setText("Click Square for {}".format(turn))
        movesLeft = movesLeft - 1


    turnLabel.setText("The Game is Over <Click> to exit")
    win.getMouse()

    """
    End Code for assignment
    """
    """
    Begin code for template
    """
    # Prints Author's name, class, and date
    print("Mark Austin")
    print("CIS 110")
    # this function will print the current date and time using asctime()
    print(time.asctime(time.localtime(time.time())))
    """
    End Code for template
    """


if __name__ == "__main__":
    main()
