"""
Program name:   CIS110 Program Template + Hello World
Program Description: Prints Hello World and uses the template for CIS assignments
Author: Mark Austin
Date created: January 30, 2021

Notes: Uses the python time library

"""
import time

class Stack:
    def __init__(self):
        self.items = []  #Create an empty stack

    def isEmpty(self):
        # Check to see if stack is empty, returns true if stack is empty
        if len(self.items) < 1:
            return True

    def push(self, item):
        # Pushes the item onto the top of the stack.
        self.items.insert(0, item)

    def pop(self):
        # Removes the top-most item from the stack and returns it.
        return self.items.pop(0)

    def peek(self):
        # 	Examines the stack returning a list of items on the stack
        return self.items

    def size(self):
        # Returns a count of the number of items in the stack.
        return len(self.items)


def main():
    """
    Begin Code for assignment

    """
    action = ''
    s = Stack()
    printWelcome()
    poppedlst = []
    while action != 'quit':
        print(s.peek(), "\n")
        action, popped = getInput(s)
        if action == 'pop':
            poppedlst.append(popped)

    print("The popped values are: ", poppedlst)


    """
    End Code for assignment
    """
    """
    Begin code for template
    """
    # Prints Author's name, class, and date
    print("Mark Austin")
    print("CIS 110")
    # this function will print the current date and time using asctime()
    print(time.asctime(time.localtime(time.time())))
    """
    End Code for template
    """


def printWelcome():
    print("this program will allow you to add items to the top of a stack")
    print("or remove the top item and display its value")


def getInput(s):
    print('enter "push x" (x=item to add) to add to list')
    print("enter pop to display the last entered value and remove")
    action = list(input("Please enter action and number if pushing: ").split(" "))

    if action[0].lower() == 'push':
        s.push(action[1])
        return action[0].lower(), None
    elif action[0].lower() == 'pop':
        if s.isEmpty():
            print("no items to pop")
            return action[0].lower(), None
        else:
            popped = s.pop()
            print("popped ", popped)
            return action[0].lower(), popped
    elif action[0].lower() == 'quit':
        return action[0].lower(), None


if __name__ == "__main__":
    main()
