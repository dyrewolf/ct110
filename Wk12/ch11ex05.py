#ch11ex05.py
# recreate list functions

def count(myList, x):
    ans = 0
    for item in myList:
        if item == x:
            ans = ans +1
    return ans

def isin(myList, x):
    for item in myList:
        if item == x:
            return True
    return False

def index(myList, x):
    for i in range(len(myList)):
        if myList[i] == x:
            return i
    return None

def reverse(lst):
    for i in range(len(lst) // 2):
        j = -(i+1)
        lst[i], lst[j] = lst[j], lst[i]

def main():
    a = [27, 64, 60, 13, 20, 62, 64, 17, 80, 58, 71, 90]
    print(count(a, 64))
    print(count(a, 11))
    print(isin(a, 17))
    print(isin(a, 23))
    print(index(a, 20))
    print(a)
    reverse(a)
    print(a)

if __name__ == '__main__': main()