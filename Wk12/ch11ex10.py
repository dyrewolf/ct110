#ch11ex10.py
#seive function for prime numbers up to n

def sieve(n):
    #initial, all values are in the sieve of candidates
    candidates = list(range(2, n+1))
    primes = [1]  # 1 is a prime number!
    while candidates != []:
        nextPrime = candidates.pop(0) # remove the first item
        primes.append(nextPrime)
        idx = 0
        while idx < len(candidates):
            if candidates[idx] % nextPrime == 0:
                candidates.pop(idx)
            else:
                idx += 1
    return primes


def main():
    print("Sieve of Eratosthenes \n")
    n = int(input("Enter upper limit:"))
    primes = sieve(n)
    print(primes)


if __name__ == '__main__': main()
