#ch11ex29.py
#using Dictionaries

class Senator():
    def __init__(self, district, name, email):
        self.district = district
        self.name = name
        self.email = email

    def districtOf(self):
        return self.district

    def nameOf(self):
        return self.name

    def emailOf(self):
        return self.email


def main():
    file = open('MaineSenate.csv', 'r')
    file.readline()
    senators = {}
    for line in file:
        data = line.split(',')
        data[0] = int(data[0]) # convert district to an integer
        senators[data[0]] = (Senator(data[0],
                                     data[4] + ", " + data[3],
                                     data[-1]))
    file.close()

    print()
    senatelist = list(senators.items())
    senatelist.sort()
    for senator in senatelist:
        print("{:2d} {}".format(senator[0], senator[1].nameOf()))

    print()
    senatelist = list(senators.values())
    senatelist.sort(key=Senator.nameOf)
    for senator in senatelist:
        print("{:2d} {}".format(senator.districtOf(), senator.nameOf()))

    print()
    x = int(input("Which District would you like to see? "))
    print("{:2d} {} {}".format(x, senators[x].nameOf(), senators[x].emailOf()))


if __name__ == '__main__': main()